BOSOM Calculator: A Breast Cancer Outcome - Survival Online Measurement Calculator using Data Mining and Predictive Modeling on SEER data
---

A project by Giltroy Meren

## Overview

The published paper for this application is [available here](http://cas.upm.edu.ph:8080/xmlui/handle/123456789/41). This is a new version of [`bosom`](https://bitbucket.org/giltroymeren/bosom) with Maven dependencies set.

## Installation

### Eclipse

* Checkout the project in your machine. 
* Go to "File" → "Import..." → "Maven" → "Existing Maven Projects" and set the location of the checkout in "Root Directory".