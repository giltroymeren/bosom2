package com.giltroymeren.bosom2.pdf;

import java.awt.Graphics2D;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.JFreeChart;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Controller;

import com.giltroymeren.bosom2.domain.WekaData;
import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

@Controller
public class PdfBuilder {

	protected final static Log logger = LogFactory.getLog(PdfBuilder.class);

	private static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat
			.forPattern("hh:mm:ss a, dd MMMM yyyy");
	private static DateTime TIME_NOW_RAW = DateTime.now();
	private static String TIME_NOW_STR = TIME_NOW_RAW
			.toString(DATE_TIME_FORMATTER);

	private static BaseColor COLOR_BOOT_BLACK_3 = new BaseColor(85, 85, 85);
	private static BaseColor COLOR_BOOT_GREY = new BaseColor(221, 221, 221);
	private static BaseColor COLOR_BOOT_BLUE_LIGHT = new BaseColor(146, 188,
			224);
	private static BaseColor COLOR_BOOT_RED_LIGHT = new BaseColor(235, 165, 163);

	private static BaseColor COLOR_GREY = new BaseColor(160, 160, 160);

	private static Font DOC_TITLE_HEAD = new Font(FontFamily.HELVETICA, 20,
			Font.BOLD, COLOR_BOOT_GREY);
	private static Font DOC_TITLE_SUB_HEAD = new Font(FontFamily.HELVETICA, 14,
			Font.NORMAL, COLOR_BOOT_BLUE_LIGHT);
	private static Font DOC_TITLE_SPECIAL = new Font(FontFamily.HELVETICA, 20,
			Font.BOLD, COLOR_BOOT_RED_LIGHT);
	private static Font DOC_LIST_HEAD = new Font(FontFamily.HELVETICA, 16,
			Font.BOLD, COLOR_BOOT_BLACK_3);
	private static Font DOC_TEXT_REG = new Font(FontFamily.HELVETICA, 10,
			Font.NORMAL, COLOR_BOOT_BLACK_3);
	private static Font DOC_TABLE_REG = new Font(FontFamily.HELVETICA, 8,
			Font.NORMAL, COLOR_BOOT_BLACK_3);
	private static Font DOC_TABLE_BOLD = new Font(FontFamily.HELVETICA, 8,
			Font.BOLD, COLOR_BOOT_BLACK_3);
	private static Font DOC_TEXT_SUBDUED = new Font(FontFamily.HELVETICA, 8,
			Font.NORMAL, COLOR_GREY);

	private static LineSeparator LINE_SEPARATOR = new LineSeparator(0.5f, 100f,
			COLOR_BOOT_GREY, 3, 0.5f);

	private static float LINE_HEIGHT = 12.0f;

	private static int WIDTH_GRAPH = 350;
	private static int HEIGHT_GRAPH = 230;

	public static void assemble(Document document, PdfWriter pdfWriter,
			WekaData wekaData) throws DocumentException {
		addMetaData(document);
		addTitlePage(document);
		addResultsSection(document, pdfWriter, wekaData);
	}

	private static void addMetaData(Document document) {
		document.addTitle("BOSOM Calculator Report");
		document.addSubject("Breast cancer survival prediction");
		document.addKeywords("Breast cancer, data mining, survival, prediction, machine learning");
		document.addAuthor("Guest " + TIME_NOW_RAW.toString("ddMMyyyyHHmmss"));
		document.addCreator("Troy Meren");
	}

	private static void addTitlePage(Document document)
			throws DocumentException {
		Paragraph paragraph = new Paragraph("BOSOM", DOC_TITLE_HEAD);
		paragraph.setSpacingAfter(1f);
		document.add(paragraph);

		paragraph = new Paragraph(
				"Breast Cancer Outcome - Survival Online Measurement Calculator",
				DOC_TITLE_SUB_HEAD);
		paragraph.setSpacingAfter(3f);
		document.add(paragraph);

		paragraph = new Paragraph("Generated on: " + TIME_NOW_STR,
				DOC_TEXT_SUBDUED);
		document.add(paragraph);

		document.add(Chunk.NEWLINE);
		document.add(LINE_SEPARATOR);

		paragraph = new Paragraph("CALCULATOR RESULTS REPORT",
				DOC_TITLE_SPECIAL);
		paragraph.setAlignment(Element.ALIGN_CENTER);
		paragraph.setSpacingAfter(15f);

		document.add(paragraph);
	}

	private static void addResultsSection(Document document,
			PdfWriter pdfWriter, WekaData wekaData) throws DocumentException {
		addEnteredDataSection(document, wekaData);
		addPredictedSurvivalSection(document, wekaData);
		addGraphPredictedSurvival(document, pdfWriter, wekaData);
	}

	private static void addEnteredDataSection(Document document,
			WekaData wekaData) throws DocumentException {

		Paragraph title = new Paragraph();
		title.add(new Paragraph("Entered data", DOC_LIST_HEAD));
		title.setSpacingAfter(10f);
		document.add(title);

		Paragraph text = new Paragraph();
		text.add(new Paragraph(
				"Here are the breast cancer values you provided in the calculator.",
				DOC_TEXT_REG));
		text.setLeading(LINE_HEIGHT);
		text.setSpacingAfter(5f);
		document.add(text);

		PdfPTable table = new PdfPTable(3);
		float[] columnWidths = { 5, 35, 60 };
		table.setWidthPercentage(100f);
		table.setWidths(columnWidths);

		PdfPCell cell = new PdfPCell(new Phrase("#", DOC_TABLE_BOLD));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorderColor(COLOR_BOOT_GREY);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase("Variable", DOC_TABLE_BOLD));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorderColor(COLOR_BOOT_GREY);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase("Value provided", DOC_TABLE_BOLD));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorderColor(COLOR_BOOT_GREY);

		table.addCell(cell);
		table.setHeaderRows(1);

		String[] enteredDataVars = { "Age at time of diagnosis",
				"Race of patient", "Stage of cancer", "Spread of etastasis",
				"Details of cancer-directed surgery", "Extension of primary tumor" };

		String[] enteredDataVals = { wekaData.getAgeDiagNum().toString(),
				wekaData.getRaceGroup(), wekaData.getStage3(),
				wekaData.getM3(), wekaData.getReasonNoCancerSurg(),
				wekaData.getM3() };

		for (int i = 0; i < enteredDataVars.length; i++) {
			cell = new PdfPCell(new Phrase(i + 1 + "", DOC_TABLE_REG));
			cell.setBorderColor(COLOR_BOOT_GREY);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(enteredDataVars[i], DOC_TABLE_REG));
			cell.setBorderColor(COLOR_BOOT_GREY);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(enteredDataVals[i], DOC_TABLE_REG));
			cell.setBorderColor(COLOR_BOOT_GREY);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);
		}

		document.add(table);

		text = new Paragraph();
		text.setSpacingAfter(5f);
		document.add(text);

		text = new Paragraph();
		text.add(new Paragraph(
				"They are used to calculate the predicted survival rate given in the other sections.",
				DOC_TEXT_REG));
		text.setLeading(LINE_HEIGHT);
		text.setSpacingAfter(25f);
		document.add(text);
	}

	private static void addPredictedSurvivalSection(Document document,
			WekaData wekaData) throws DocumentException {

		Paragraph title = new Paragraph();
		title.add(new Paragraph("Predicted survival", DOC_LIST_HEAD));
		title.setSpacingAfter(10f);
		document.add(title);

		Paragraph text = new Paragraph();
		text.add(new Paragraph(
				"Here are the predicted survivals as determined by our models based from "
						+ "past breast cancer patient records. "
						+ "These are from two to ten years, with two years of interval for uniformity.",
				DOC_TEXT_REG));
		text.setSpacingAfter(5f);
		text.setLeading(LINE_HEIGHT);
		document.add(text);

		PdfPTable table = new PdfPTable(2);
		float[] columnWidths = { 50, 50 };
		table.setWidthPercentage(100f);
		table.setWidths(columnWidths);

		PdfPCell cell = new PdfPCell(new Phrase("Time period", DOC_TABLE_BOLD));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorderColor(COLOR_BOOT_GREY);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase("Survival", DOC_TABLE_BOLD));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorderColor(COLOR_BOOT_GREY);
		table.addCell(cell);

		table.setHeaderRows(1);

		String[] predictions = { wekaData.getTime2().toString(),
				wekaData.getTime4().toString(), wekaData.getTime6().toString(),
				wekaData.getTime8().toString(), wekaData.getTime10().toString() };

		for (int i = 1; i <= predictions.length; i++) {
			cell = new PdfPCell(new Phrase(i * 2 + " years", DOC_TABLE_REG));
			cell.setBorderColor(COLOR_BOOT_GREY);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(predictions[i - 1] + "%",
					DOC_TABLE_REG));
			cell.setBorderColor(COLOR_BOOT_GREY);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);
		}

		document.add(table);

		text = new Paragraph();
		text.setSpacingAfter(5f);
		document.add(text);

		text = new Paragraph();
		text.add(new Paragraph(
				"Some of the values for each time period might not conform to the "
						+ "inverse relationship of survival prediction and time due to the data used.",
				DOC_TEXT_REG));
		text.setSpacingAfter(25f);
		text.setLeading(LINE_HEIGHT);
		document.add(text);
	}

	private static void addGraphPredictedSurvival(Document document,
			PdfWriter pdfWriter, WekaData wekaData) throws DocumentException {
		Paragraph title = new Paragraph();
		title.add(new Paragraph("Graph of predicted survival", DOC_LIST_HEAD));
		title.setSpacingAfter(10f);
		document.add(title);

		Paragraph text = new Paragraph();
		text.add(new Paragraph(
				"Here is a chart representation of the predicted survival computed by our models.",
				DOC_TEXT_REG));
		text.setLeading(LINE_HEIGHT);
		text.setSpacingAfter(10f);
		document.add(text);

		addBarChartToPdf(wekaData, pdfWriter);
	}

	// http://www.wirelust.com/2008/03/17/creating-an-itext-pdf-with-embedded-jfreechart/
	private static void addBarChartToPdf(WekaData wekaData, PdfWriter pdfWriter) {
		logger.info("Start of charts creation");

		JFreeChart chart = ChartBuilder.createBarChart(wekaData);

		PdfContentByte dc = pdfWriter.getDirectContent();

		PdfTemplate tp = dc.createTemplate(800, HEIGHT_GRAPH);
		@SuppressWarnings("deprecation")
		Graphics2D g2 = tp.createGraphics(800, HEIGHT_GRAPH,
				new DefaultFontMapper());

		java.awt.geom.Rectangle2D r2D = new java.awt.geom.Rectangle2D.Double(
				75, 0, WIDTH_GRAPH, HEIGHT_GRAPH);
		chart.draw(g2, r2D);
		g2.dispose();

		dc.addTemplate(tp, 38, pdfWriter.getVerticalPosition(true)
				- HEIGHT_GRAPH);

		logger.info("End of charts creation");
	}

}