package com.giltroymeren.bosom2.pdf;

/*
 * This class is part of the book "iText in Action - 2nd Edition"
 * written by Bruno Lowagie (ISBN: 9781935182610)
 * For more info, go to: http://itextpdf.com/examples/
 * This example only works with the AGPL version of iText.
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;

public class PdfConcatenator {

	protected final static Log logger = LogFactory.getLog(PdfConcatenator.class);
	
	/**
	 * Main method.
	 * 
	 * @param args
	 *            no arguments needed
	 * @throws DocumentException
	 * @throws IOException
	 * @throws SQLException
	 */
	public static String concatenate(String concatPath, String... files) throws IOException,
			DocumentException, SQLException {

		logger.info("Preparation for PDF concatenation \n");
		
		for(int i = 0; i < files.length; i++) {
			logger.info("File # " + i + ": " + files[i]);
		}
		
		Document document = new Document();
		PdfCopy copy = new PdfCopy(document, new FileOutputStream(concatPath));
		document.open();
		PdfReader reader;
		int n;

		for (int i = 0; i < files.length; i++) {
			reader = new PdfReader(files[i]);
			n = reader.getNumberOfPages();
			for (int page = 0; page < n;) {
				copy.addPage(copy.getImportedPage(reader, ++page));
			}
			copy.freeReader(reader);
			reader.close();
		}
		document.close();

		return concatPath;
	}
}