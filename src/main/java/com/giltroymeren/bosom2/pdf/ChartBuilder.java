package com.giltroymeren.bosom2.pdf;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Paint;
import java.math.BigDecimal;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;

import com.giltroymeren.bosom2.domain.WekaData;
import com.itextpdf.text.Font;

public class ChartBuilder {

	protected static JFreeChart createBarChart(WekaData wekaData) {

		BigDecimal[] dataset = { wekaData.getTime2(), wekaData.getTime4(),
				wekaData.getTime6(), wekaData.getTime8(), wekaData.getTime10() };

		// http://www.wirelust.com/2008/03/17/creating-an-itext-pdf-with-embedded-jfreechart/
		DefaultCategoryDataset chartData = new DefaultCategoryDataset();
		for (int i = 0; i < dataset.length; i++) {
			chartData.setValue(dataset[i], "Population", (i + 1) * 2 + "");
		}

		JFreeChart chart = ChartFactory.createBarChart("",
				"Time period (years)", "Predicted survival (%)", chartData,
				PlotOrientation.VERTICAL, false, true, false);
		chart.setBackgroundPaint(Color.WHITE);
		ChartFactory.setChartTheme(StandardChartTheme.createLegacyTheme());

		final CategoryPlot plot = chart.getCategoryPlot();
		((BarRenderer) plot.getRenderer())
				.setBarPainter(new StandardBarPainter());

		plot.setBackgroundPaint(Color.WHITE);

		plot.setDomainGridlinesVisible(true);
		plot.setRangeGridlinesVisible(true);

		plot.setDomainGridlineStroke(new BasicStroke(0.25f));
		plot.setRangeGridlineStroke(new BasicStroke(0.25f));

		plot.setDomainGridlinePaint(new Color(204, 204, 204));
		plot.setRangeGridlinePaint(new Color(204, 204, 204));

		java.awt.Font fontGraphLabel = new java.awt.Font("Helvetica",
				Font.NORMAL, 8);
		java.awt.Font fontGraphTicks = new java.awt.Font("Helvetica",
				Font.NORMAL, 6);
		plot.getDomainAxis().setLabelFont(fontGraphLabel);
		plot.getRangeAxis().setLabelFont(fontGraphLabel);
		plot.getDomainAxis().setTickLabelFont(fontGraphTicks);
		plot.getRangeAxis().setTickLabelFont(fontGraphTicks);

		CategoryItemRenderer categoryItemRenderer = new CustomRenderer();
		plot.setRenderer(categoryItemRenderer);

		final NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		int yLimit = (int) getRoundedUpMultipleOfTen(getHighestArrayValue(dataset));
		yAxis.setRange(0, yLimit);
		if (yLimit > 40) {
			yAxis.setTickUnit(new NumberTickUnit(10));
		} else {
			yAxis.setTickUnit(new NumberTickUnit(5));
		}
		
		final BarRenderer barRenderer = (BarRenderer) plot.getRenderer();
		barRenderer.setDrawBarOutline(true);
		barRenderer.setShadowVisible(false);

		barRenderer.setSeriesOutlinePaint(0, new Color(0, 0, 0));
		barRenderer.setSeriesOutlineStroke(0, new BasicStroke(2f));

		return chart;
	}

	private static class CustomRenderer extends BarRenderer {

		private static final long serialVersionUID = 6826676370155152948L;
		private Paint[] colors;
		int transparency = 95;

		// http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/
		public CustomRenderer() {
			this.colors = new Paint[] { new Color(1, 158, 115, transparency),
					new Color(240, 228, 66, transparency),
					new Color(0, 114, 178, transparency),
					new Color(213, 94, 0, transparency),
					new Color(204, 121, 167, transparency) };
		}

		public Paint getItemPaint(final int row, final int column) {
			return this.colors[column % this.colors.length];
		}
	}

	private static double getHighestArrayValue(BigDecimal[] dataset) {
		double max = 0;
		for (int i = 1; i < dataset.length; i++) {
			if (dataset[i].doubleValue() > max) {
				max = dataset[i].doubleValue();
			}
		}
		return max;
	}

	private static double getRoundedUpMultipleOfTen(double number) {
		return ((number + 9) / 10) * 10;
	}
}
