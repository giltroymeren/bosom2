package com.giltroymeren.bosom2.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

@SuppressWarnings("serial")
public class WekaData implements Serializable {

	@NotNull
	@Range(min = 1, max = 150)
	private Integer ageDiagNum;

	@NotEmpty
	private String raceGroup;

	@NotEmpty
	private String stage3;

	@NotEmpty
	private String m3;

	@NotEmpty
	private String reasonNoCancerSurg;

	@NotEmpty
	private String ext2;

	private BigDecimal time2;
	private BigDecimal time4;
	private BigDecimal time6;
	private BigDecimal time8;
	private BigDecimal time10;

	public Integer getAgeDiagNum() {
		return ageDiagNum;
	}

	public void setAgeDiagNum(Integer ageDiagNum) {
		this.ageDiagNum = ageDiagNum;
	}

	public String getRaceGroup() {
		return raceGroup;
	}

	public void setRaceGroup(String raceGroup) {
		this.raceGroup = raceGroup;
	}

	public String getStage3() {
		return stage3;
	}

	public void setStage3(String stage3) {
		this.stage3 = stage3;
	}

	public String getM3() {
		return m3;
	}

	public void setM3(String m3) {
		this.m3 = m3;
	}

	public String getReasonNoCancerSurg() {
		return reasonNoCancerSurg;
	}

	public void setReasonNoCancerSurg(String reasonNoCancerSurg) {
		this.reasonNoCancerSurg = reasonNoCancerSurg;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}

	public BigDecimal getTime2() {
		return time2;
	}

	public void setTime2(BigDecimal time2) {
		this.time2 = time2;
	}

	public BigDecimal getTime4() {
		return time4;
	}

	public void setTime4(BigDecimal time4) {
		this.time4 = time4;
	}

	public BigDecimal getTime6() {
		return time6;
	}

	public void setTime6(BigDecimal time6) {
		this.time6 = time6;
	}

	public BigDecimal getTime8() {
		return time8;
	}

	public void setTime8(BigDecimal time8) {
		this.time8 = time8;
	}

	public BigDecimal getTime10() {
		return time10;
	}

	public void setTime10(BigDecimal time10) {
		this.time10 = time10;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ageDiagNum == null) ? 0 : ageDiagNum.hashCode());
		result = prime * result + ((ext2 == null) ? 0 : ext2.hashCode());
		result = prime * result + ((m3 == null) ? 0 : m3.hashCode());
		result = prime * result + ((raceGroup == null) ? 0 : raceGroup.hashCode());
		result = prime * result + ((reasonNoCancerSurg == null) ? 0 : reasonNoCancerSurg.hashCode());
		result = prime * result + ((stage3 == null) ? 0 : stage3.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WekaData other = (WekaData) obj;
		if (ageDiagNum == null) {
			if (other.ageDiagNum != null)
				return false;
		} else if (!ageDiagNum.equals(other.ageDiagNum))
			return false;
		if (ext2 == null) {
			if (other.ext2 != null)
				return false;
		} else if (!ext2.equals(other.ext2))
			return false;
		if (m3 == null) {
			if (other.m3 != null)
				return false;
		} else if (!m3.equals(other.m3))
			return false;
		if (raceGroup == null) {
			if (other.raceGroup != null)
				return false;
		} else if (!raceGroup.equals(other.raceGroup))
			return false;
		if (reasonNoCancerSurg == null) {
			if (other.reasonNoCancerSurg != null)
				return false;
		} else if (!reasonNoCancerSurg.equals(other.reasonNoCancerSurg))
			return false;
		if (stage3 == null) {
			if (other.stage3 != null)
				return false;
		} else if (!stage3.equals(other.stage3))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WekaData [ageDiagNum=" + ageDiagNum + ", raceGroup=" + raceGroup + ", stage3=" + stage3 + ", m3=" + m3
				+ ", reasonNoCancerSurg=" + reasonNoCancerSurg + ", ext2=" + ext2 + ", time2=" + time2 + ", time4="
				+ time4 + ", time6=" + time6 + ", time8=" + time8 + ", time10=" + time10 + "]";
	}

}