package com.giltroymeren.bosom2.util;

public class Constants {
	public static final String VIEW_KEY_TTILE = "title";
	public static final String VIEW_KEY_PAGE_NAME = "pageName";
	public static final String VIEW_KEY_PAGE_TITLE_HEADER = "pageTitleHeader";
	public static final String VIEW_KEY_PAGE_TITLE_SUBHEADER = "pageTitleSubheader";
	public static final String VIEW_KEY_PAGE_ALERT_STRONG_CONTENT = "alertStrongContent";
	public static final String VIEW_KEY_PAGE_ALERT_CONTENT = "alertContent";
	public static final String VIEW_KEY_PAGE_ALERT_TYPE = "alertType";
	public static final String VIEW_KEY_PAGE_FLOT = "isFlotUsed";
	public static final String VIEW_KEY_PAGE_WEKADATA = "wekaData";
	public static final String VIEW_KEY_PAGE_PREDICTIONS_MAP = "predictionsMap";
	public static final String VIEW_KEY_PAGE_PDF_LOCATION = "pdfLocation";

	public static final String URL_ABOUT_BOSOM = "about/bosom";
	public static final String URL_ABOUT_SITE = "about/site";
	public static final String URL_CALC_FORM = "calc/form";
	public static final String URL_CALC_RESULTS = "calc/results";
	public static final String URL_SUPPLEMENTS = "supplements";

	public static final String VIEW_TITLE_ABOUT_BOSOM = "About - BOSOM Calculator";
	public static final String VIEW_PAGE_NAME_ABOUT = "about";
	public static final String VIEW_PAGE_TITLE_HEADER_ABOUT_BOSOM = "Calculating Breast Cancer";
	public static final String VIEW_PAGE_TITLE_SUBHEADER_ABOUT_BOSOM = "Learn more about the BOSOM Calculator and its components";

	public static final String VIEW_TITLE_ABOUT_SITE = "About - Site";
	public static final String VIEW_PAGE_TITLE_HEADER_ABOUT_SITE = "Site General Information";
	public static final String VIEW_PAGE_TITLE_SUBHEADER_ABOUT_SITE = "Technical information and acknowledgements to the site's backbone technology";

	public static final String VIEW_TITLE_CALC = "Calculator";
	public static final String VIEW_PAGE_NAME_CALC = "calc";
	public static final String VIEW_PAGE_TITLE_HEADER_CALC = "BOSOM Calculator";
	public static final String VIEW_PAGE_TITLE_SUBHEADER_CALC = "Evaluate your survival prediction";

	public static final String VIEW_PAGE_ALERT_STRONG_CONTENT = "You have errors in the form.";
	public static final String VIEW_PAGE_ALERT_CONTENT = "Please review the information you provided before submission.";
	public static final String VIEW_PAGE_ALERT_TYPE = "danger";

	public static final String VIEW_TITLE_CALC_RESULTS = "Results";
	public static final String VIEW_PAGE_TITLE_HEADER_CALC_RESULTS = "Predictive results";
	public static final String VIEW_PAGE_TITLE_SUBHEADER_CALC_RESULTS = "Our predictive model's interpretation of your survival.";

	public static final String VIEW_TITLE_SUPPLEMENTS = "Supplements";
	public static final String VIEW_PAGE_NAME_SUPPLEMENTS = "supplements";
	public static final String VIEW_PAGE_TITLE_HEADER_SUPPLEMENTS = "Supplemental Links";
	public static final String VIEW_PAGE_TITLE_SUBHEADER_SUPPLEMENTS = "Local and international institutions and groups dedicated "
			+ "to breast cancer research and prevention.";
}