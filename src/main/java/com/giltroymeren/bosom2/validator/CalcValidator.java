package com.giltroymeren.bosom2.validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.giltroymeren.bosom2.domain.WekaData;

public class CalcValidator implements Validator {

	protected final Log logger = LogFactory.getLog(getClass());

	@SuppressWarnings("rawtypes")
	@Override
	public boolean supports(Class clazz) {
		return WekaData.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		WekaData wekaData = (WekaData) object;
	}
}
