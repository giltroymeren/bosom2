package com.giltroymeren.bosom2.service;

import com.giltroymeren.bosom2.domain.WekaData;

import weka.core.Instances;

public interface CalcArffService {

	public Instances getInstances(WekaData wekaData);

}