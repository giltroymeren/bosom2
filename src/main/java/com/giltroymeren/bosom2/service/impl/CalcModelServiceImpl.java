package com.giltroymeren.bosom2.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.giltroymeren.bosom2.service.CalcModelService;

import weka.classifiers.Classifier;
import weka.core.Instances;

@Service("calcModelService")
public class CalcModelServiceImpl implements CalcModelService {

	protected final static Log logger = LogFactory
			.getLog(CalcModelServiceImpl.class);
	private static final String[] NAME_CLASSIFIERS = { "adt", "lb", "j48",
			"rf", "rs" };
	private static final String[] NAME_CLASSIFIERS_LOCATION = { "time2",
			"time4", "time6", "time8", "time10" };

	@Resource
	private WebServiceContext wsContext;

	public Classifier getClassifier(String filePath, HttpServletRequest request) {
		String path = "/WEB-INF/models/" + filePath + ".MODEL";

		Classifier classifier = null;

		ServletContext servletContext = request.getSession()
				.getServletContext();
		InputStream inputStream = null;
		try {
			inputStream = servletContext.getResourceAsStream(path);

			logger.info("\nCalcModelServiceImpl: reading model files \n"
					+ "Path: " + path + "\n");

			classifier = (Classifier) weka.core.SerializationHelper
					.read(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}

		return classifier;
	}

	private static Map<String, Object> predict(Instances instances,
			Classifier classifier, int attributeIndex) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		int instanceIndex = 0; // do not change, equal to row 1 of ARFF
		double[] percentage = { 0 };
		double outcomeValue = 0;

		instances.setClassIndex(attributeIndex);

		try {
			outcomeValue = classifier.classifyInstance(instances.instance(0));
			percentage = classifier.distributionForInstance(instances
					.instance(instanceIndex));
		} catch (Exception e) {
			e.printStackTrace();
		}

		map.put("Class", outcomeValue);

		double percentageRaw = percentage[1];
//		if (outcomeValue == new Double(1)) {
//			percentageRaw = percentage[1];
//		} else {
//			percentageRaw = 1 - percentage[0];
//		}

		map.put("Percentage", percentageRaw);
		logger.info("CalcModelServiceImpl: predicting class and its percentage distribution\n"
				+ "Classifier: "
				+ classifier.getClass().toString()
				+ "\n"
				+ "Class [0=Dead,1=Alive]: "
				+ outcomeValue
				+ "\n"
				+ "Percentage [0]: "
				+ percentage[0]
				+ "\n"
				+ "Percentage [1]: " + percentage[1] + "\n");

		return map;
	}

	public Map<String, Map<String, Map<String, Object>>> getPredictions(
			Instances instances, HttpServletRequest request) {
		Map<String, Map<String, Map<String, Object>>> container = new LinkedHashMap<String, Map<String, Map<String, Object>>>();
		Map<String, Map<String, Object>> content;

		for (int i = 0; i < NAME_CLASSIFIERS_LOCATION.length; i++) {
			content = new LinkedHashMap<String, Map<String, Object>>();
			for (int j = 0; j < NAME_CLASSIFIERS.length; j++) {

				String path = NAME_CLASSIFIERS_LOCATION[i] + "/"
						+ NAME_CLASSIFIERS[j];

				Map<String, Object> predictions = predict(instances,
						getClassifier(path, request), i + 6);

				content.put(NAME_CLASSIFIERS[j], predictions);
			}
			container.put(NAME_CLASSIFIERS_LOCATION[i], content);
		}

		return container;
	}
}
