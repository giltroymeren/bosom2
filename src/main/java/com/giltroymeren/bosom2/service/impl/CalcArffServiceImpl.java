package com.giltroymeren.bosom2.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.giltroymeren.bosom2.domain.WekaData;
import com.giltroymeren.bosom2.service.CalcArffService;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.Utils;

@SuppressWarnings("deprecation")
@Service("calcArffService")
public class CalcArffServiceImpl implements CalcArffService {

	protected final Log logger = LogFactory.getLog(getClass());

	/**
	 * @Source http://weka.wikispaces.com/Creating+an+ARFF+file
	 */
	public Instances getInstances(WekaData wekaData) {

		FastVector<Attribute> atts;
		Instances data;
		double[] vals;

		// 1. set up attributes
		atts = new FastVector<Attribute>();

		atts.addElement(new Attribute("ageDiagNum"));

		FastVector<String> raceGroupAttVals = new FastVector<String>();
		raceGroupAttVals.addElement("Black");
		raceGroupAttVals.addElement("Other");
		raceGroupAttVals.addElement("Unknown");
		raceGroupAttVals.addElement("White");
		atts.addElement(new Attribute("raceGroup", raceGroupAttVals));

		FastVector<String> stage3AttVals = new FastVector<String>();
		stage3AttVals.addElement("0");
		stage3AttVals.addElement("I");
		stage3AttVals.addElement("IIA");
		stage3AttVals.addElement("IIB");
		stage3AttVals.addElement("IIIA");
		stage3AttVals.addElement("IIIB");
		stage3AttVals.addElement("IIIC");
		stage3AttVals.addElement("IIINOS");
		stage3AttVals.addElement("IV");
		stage3AttVals.addElement("UNK Stage");
		atts.addElement(new Attribute("stage3", stage3AttVals));

		FastVector<String> m3AttVals = new FastVector<String>();
		m3AttVals.addElement("M0");
		m3AttVals.addElement("M1");
		m3AttVals.addElement("MX");
		atts.addElement(new Attribute("m3", m3AttVals));

		FastVector<String> reasonNoCancerSurgAttVals = new FastVector<String>();
		reasonNoCancerSurgAttVals
				.addElement("Not performed, patient died prior to recommended surgery");
		reasonNoCancerSurgAttVals.addElement("Not recommended");
		reasonNoCancerSurgAttVals
				.addElement("Not recommended, contraindicated due to other conditions");
		reasonNoCancerSurgAttVals
				.addElement("Recommended but not performed, patient refused");
		reasonNoCancerSurgAttVals
				.addElement("Recommended but not performed, unknown reason");
		reasonNoCancerSurgAttVals
				.addElement("Recommended, unknown if performed");
		reasonNoCancerSurgAttVals.addElement("Surgery performed");
		reasonNoCancerSurgAttVals
				.addElement("Unknown; death certificate or autopsy only case");
		atts.addElement(new Attribute("reasonNoCancerSurg",
				reasonNoCancerSurgAttVals));

		FastVector<String> ext2AttVals = new FastVector<String>();
		ext2AttVals.addElement("00");
		ext2AttVals.addElement("05");
		ext2AttVals.addElement("10");
		ext2AttVals.addElement("11");
		ext2AttVals.addElement("13");
		ext2AttVals.addElement("14");
		ext2AttVals.addElement("15");
		ext2AttVals.addElement("16");
		ext2AttVals.addElement("17");
		ext2AttVals.addElement("18");
		ext2AttVals.addElement("20");
		ext2AttVals.addElement("21");
		ext2AttVals.addElement("23");
		ext2AttVals.addElement("24");
		ext2AttVals.addElement("25");
		ext2AttVals.addElement("26");
		ext2AttVals.addElement("27");
		ext2AttVals.addElement("28");
		ext2AttVals.addElement("30");
		ext2AttVals.addElement("31");
		ext2AttVals.addElement("33");
		ext2AttVals.addElement("34");
		ext2AttVals.addElement("35");
		ext2AttVals.addElement("36");
		ext2AttVals.addElement("37");
		ext2AttVals.addElement("38");
		ext2AttVals.addElement("40");
		ext2AttVals.addElement("50");
		ext2AttVals.addElement("60");
		ext2AttVals.addElement("70");
		ext2AttVals.addElement("80");
		ext2AttVals.addElement("85");
		ext2AttVals.addElement("99");
		atts.addElement(new Attribute("ext2", ext2AttVals));

		FastVector<String> time2AttVals = new FastVector<String>();
		time2AttVals.addElement("0");
		time2AttVals.addElement("1");
		atts.addElement(new Attribute("time2", time2AttVals));

		FastVector<String> time4AttVals = new FastVector<String>();
		time4AttVals.addElement("0");
		time4AttVals.addElement("1");
		atts.addElement(new Attribute("time4", time4AttVals));

		FastVector<String> time6AttVals = new FastVector<String>();
		time6AttVals.addElement("0");
		time6AttVals.addElement("1");
		atts.addElement(new Attribute("time6", time6AttVals));

		FastVector<String> time8AttVals = new FastVector<String>();
		time8AttVals.addElement("0");
		time8AttVals.addElement("1");
		atts.addElement(new Attribute("time8", time8AttVals));

		FastVector<String> time10AttVals = new FastVector<String>();
		time10AttVals.addElement("0");
		time10AttVals.addElement("1");
		atts.addElement(new Attribute("time10", time10AttVals));

		// 2. create Instances object
		data = new Instances("SeerBreastCancer", atts, 0);

		// 3. fill with data
		vals = new double[data.numAttributes()];

		vals[0] = wekaData.getAgeDiagNum();
		vals[1] = raceGroupAttVals.indexOf(wekaData.getRaceGroup());
		vals[2] = stage3AttVals.indexOf(wekaData.getStage3());
		vals[3] = m3AttVals.indexOf(wekaData.getM3());
		vals[4] = reasonNoCancerSurgAttVals.indexOf(wekaData
				.getReasonNoCancerSurg());
		vals[5] = ext2AttVals.indexOf(wekaData.getExt2());
		vals[6] = Utils.missingValue();
		vals[7] = Utils.missingValue();
		vals[8] = Utils.missingValue();
		vals[9] = Utils.missingValue();
		vals[10] = Utils.missingValue();

		// add
		data.add(new DenseInstance(1.0, vals));

		logger.info("\nCalcArffServiceImpl: creating Instances data\n"
				+ data.toString() + "\n");

		return data;
	}

}
