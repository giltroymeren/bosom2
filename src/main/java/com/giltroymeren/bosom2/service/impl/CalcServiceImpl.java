package com.giltroymeren.bosom2.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giltroymeren.bosom2.domain.WekaData;
import com.giltroymeren.bosom2.service.CalcArffService;
import com.giltroymeren.bosom2.service.CalcModelService;
import com.giltroymeren.bosom2.service.CalcService;

import weka.core.Instances;

@Service("calcService")
public class CalcServiceImpl implements CalcService {

	protected final Log logger = LogFactory.getLog(getClass());

	// @Value("${dir.local}")
	// private String PATH_FILES;
	// private String PATH_FILES =
	// "C:/apache-tomcat/7.0.47/webapps/bosom/WEB-INF/classes/resources/";
	// private String PATH_FILES =
	// "/home/gtmeren/tomcat7/webapps/bosom/WEB-INF/classes/resources/";

	private CalcArffService calcArffService;
	private CalcModelService calcModelService;

	@Autowired
	public void setCalcArffService(CalcArffService calcArffService) {
		this.calcArffService = calcArffService;
	}

	@Autowired
	public void setCalcModelService(CalcModelService calcModelService) {
		this.calcModelService = calcModelService;
	}

	@Override
	public Map<String, Map<String, Map<String, Object>>> evaluate(WekaData wekaData, HttpServletRequest request) {

		Instances instances = calcArffService.getInstances(wekaData);

		Map<String, Map<String, Map<String, Object>>> predictions = calcModelService
				.getPredictions(instances, request);
		Map<String, Double> meanMap = getMeanPredictions(predictions);

		wekaData.setTime2(new BigDecimal(meanMap.get("time2")).setScale(2,
				BigDecimal.ROUND_HALF_UP));
		wekaData.setTime4(new BigDecimal(meanMap.get("time4")).setScale(2,
				BigDecimal.ROUND_HALF_UP));
		wekaData.setTime6(new BigDecimal(meanMap.get("time6")).setScale(2,
				BigDecimal.ROUND_HALF_UP));
		wekaData.setTime8(new BigDecimal(meanMap.get("time8")).setScale(2,
				BigDecimal.ROUND_HALF_UP));
		wekaData.setTime10(new BigDecimal(meanMap.get("time10")).setScale(2,
				BigDecimal.ROUND_HALF_UP));
		
		return predictions;
	}

	private Map<String, Double> getMeanPredictions(
			Map<String, Map<String, Map<String, Object>>> map) {
		Map<String, Double> meanMap = new LinkedHashMap<String, Double>();
		BigDecimal meanPrediction = new BigDecimal(0);

		for (Map.Entry<String, Map<String, Map<String, Object>>> entry1 : map
				.entrySet()) {
			Map<String, Map<String, Object>> entry1Map = entry1.getValue();

			logger.info("\nCalcServiceImpl: extracting data per time period\n"
					+ "Time Period: " + entry1.getKey() + "\n" + "Data: "
					+ entry1Map + "\n");

			double sumPercentages = 0;
			for (Map.Entry<String, Map<String, Object>> entry2 : entry1Map
					.entrySet()) {
				HashMap<String, Object> entry2Map = (HashMap<String, Object>) entry2
						.getValue();
				sumPercentages += (Double) entry2Map.get("Percentage");
			}

			meanPrediction = new BigDecimal(
					String.valueOf((sumPercentages / 5) * 100)).setScale(2,
					BigDecimal.ROUND_HALF_UP);

			logger.info("\nCalcServiceImp: computing prediction means"
					+ "Time Period: " + entry1.getKey() + "\n" + "Sum: "
					+ (sumPercentages * 100) + "\n" + "Mean: "
					+ meanPrediction.doubleValue() + "\n");

			meanMap.put(entry1.getKey(), meanPrediction.doubleValue());
		}
		return meanMap;
	}

}
