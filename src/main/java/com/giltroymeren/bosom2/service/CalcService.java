package com.giltroymeren.bosom2.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.giltroymeren.bosom2.domain.WekaData;

public interface CalcService {

	public Map<String, Map<String, Map<String, Object>>> evaluate(
			WekaData wekaData, HttpServletRequest request);

}
