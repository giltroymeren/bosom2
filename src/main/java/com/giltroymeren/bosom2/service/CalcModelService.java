package com.giltroymeren.bosom2.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import weka.classifiers.Classifier;
import weka.core.Instances;

public interface CalcModelService {

	public Classifier getClassifier(String filePath, HttpServletRequest request);

	public Map<String, Map<String, Map<String, Object>>> getPredictions(
			Instances instances, HttpServletRequest request);

}