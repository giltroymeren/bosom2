package com.giltroymeren.bosom2.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;

import com.giltroymeren.bosom2.domain.WekaData;
import com.giltroymeren.bosom2.pdf.PdfBuilder;
import com.giltroymeren.bosom2.pdf.PdfConcatenator;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

@Controller
public class PdfController {

	protected final static Log logger = LogFactory.getLog(PdfController.class);

	private static DateTime TIME_NOW_RAW = DateTime.now();
	private static String NAME_DOCUMENT = TIME_NOW_RAW.toString("ddMMyyyyHHmmss");
	private static String NAME_FILE_ORIG = "ORIG-" + NAME_DOCUMENT + ".pdf";
	private static String NAME_FILE_CONCAT = NAME_DOCUMENT + ".pdf";

	public static String getPdfFilePath(WekaData wekaData, HttpServletRequest request, HttpServletResponse response)
			throws DocumentException, IOException, SQLException {
		logger.info("Start of PDF creation");

		Document document = new Document(PageSize.A4);
		PdfWriter pdfWriter = null;
		try {
			pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(getPdfFile(request, NAME_FILE_ORIG)));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		document.open();
		PdfBuilder.assemble(document, pdfWriter, wekaData);
		document.close();
		pdfWriter.close();

		PdfConcatenator.concatenate(getPdfFile(request, NAME_FILE_CONCAT).getAbsolutePath(),
				getPdfFile(request, NAME_FILE_ORIG).getAbsolutePath(),
				getPdfFile(request, "/bosom-info.pdf").getAbsolutePath());

		logger.info("End of PDF creation");

		/*
		 * file.getAbsolutePath().toString(); currently mapped to /calc/reports/
		 * via spring-servlet.xml
		 */
		return "calc/reports/" + NAME_FILE_CONCAT;
	}

	public static File getPdfFile(HttpServletRequest request, String fileName) {
		ServletContext servletContext = request.getSession().getServletContext();
		String filePath = servletContext.getRealPath("/WEB-INF/reports/") + "/" + fileName;
		File file = new File(filePath);
		file.deleteOnExit();
		logger.info("PDF file path: " + file.getAbsolutePath().toString());
		return file;
	}

}