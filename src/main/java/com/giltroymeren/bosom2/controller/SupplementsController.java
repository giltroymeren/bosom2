package com.giltroymeren.bosom2.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.giltroymeren.bosom2.util.Constants;

@Controller
public class SupplementsController {

	protected final static Log logger = LogFactory.getLog(SupplementsController.class);

	@RequestMapping(value = "/supplements", method = RequestMethod.GET)
	public String showSupplementsPage(ModelMap modelMap) {
		modelMap.addAttribute(Constants.VIEW_KEY_TTILE, Constants.VIEW_TITLE_SUPPLEMENTS);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_NAME, Constants.VIEW_PAGE_NAME_SUPPLEMENTS);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_HEADER, Constants.VIEW_PAGE_TITLE_HEADER_SUPPLEMENTS);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER, Constants.VIEW_PAGE_TITLE_SUBHEADER_SUPPLEMENTS);

		return Constants.URL_SUPPLEMENTS;
	}

}