package com.giltroymeren.bosom2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.giltroymeren.bosom2.util.Constants;

@Controller
public class AboutController {

	@RequestMapping(value = { "/about", "/about/bosom" })
	public String showAboutBosomPage(ModelMap model) {
		model.addAttribute(Constants.VIEW_KEY_TTILE, Constants.VIEW_TITLE_ABOUT_BOSOM);
		model.addAttribute(Constants.VIEW_KEY_PAGE_NAME, Constants.VIEW_PAGE_NAME_ABOUT);
		model.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_HEADER, Constants.VIEW_PAGE_TITLE_HEADER_ABOUT_BOSOM);
		model.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER, Constants.VIEW_PAGE_TITLE_SUBHEADER_ABOUT_BOSOM);
		return Constants.URL_ABOUT_BOSOM;
	}

	@RequestMapping(value = "/about/site")
	public String showAboutSitePage(ModelMap model) {
		model.addAttribute(Constants.VIEW_KEY_TTILE, Constants.VIEW_TITLE_ABOUT_SITE);
		model.addAttribute(Constants.VIEW_KEY_PAGE_NAME, Constants.VIEW_PAGE_NAME_ABOUT);
		model.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_HEADER, Constants.VIEW_PAGE_TITLE_HEADER_ABOUT_SITE);
		model.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER, Constants.VIEW_PAGE_TITLE_SUBHEADER_ABOUT_SITE);
		return Constants.URL_ABOUT_SITE;
	}

}
