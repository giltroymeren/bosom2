package com.giltroymeren.bosom2.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.giltroymeren.bosom2.domain.WekaData;
import com.giltroymeren.bosom2.service.CalcService;
import com.giltroymeren.bosom2.util.Constants;

@Controller
public class CalcController {

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private CalcService calcService;

	@Autowired
	private PdfController pdfController;

	@RequestMapping(value = "/calc", method = RequestMethod.GET)
	public String showGet(@ModelAttribute("wekaData") WekaData wekaData, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		modelMap.addAttribute(Constants.VIEW_KEY_TTILE, Constants.VIEW_TITLE_CALC);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_NAME, Constants.VIEW_PAGE_NAME_CALC);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_HEADER, Constants.VIEW_PAGE_TITLE_HEADER_CALC);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER, Constants.VIEW_PAGE_TITLE_SUBHEADER_CALC);

		return Constants.URL_CALC_FORM;
	}

	@SuppressWarnings("static-access")
	@RequestMapping(value = "/calc", method = RequestMethod.POST)
	public String showPost(@ModelAttribute("wekaData") @Valid WekaData wekaData, final BindingResult bindingResult,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_NAME, Constants.VIEW_PAGE_NAME_CALC);

		if (bindingResult.hasErrors()) {
			modelMap.addAttribute(Constants.VIEW_KEY_TTILE, Constants.VIEW_TITLE_CALC);
			modelMap.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_HEADER, Constants.VIEW_PAGE_TITLE_HEADER_CALC);
			modelMap.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER, Constants.VIEW_PAGE_TITLE_SUBHEADER_CALC);
			modelMap.addAttribute(Constants.VIEW_KEY_PAGE_ALERT_STRONG_CONTENT,
					Constants.VIEW_PAGE_ALERT_STRONG_CONTENT);
			modelMap.addAttribute(Constants.VIEW_KEY_PAGE_ALERT_CONTENT, Constants.VIEW_PAGE_ALERT_CONTENT);
			modelMap.addAttribute(Constants.VIEW_KEY_PAGE_ALERT_TYPE, Constants.VIEW_PAGE_ALERT_TYPE);

			return Constants.URL_CALC_FORM;
		}

		modelMap.addAttribute(Constants.VIEW_KEY_TTILE, Constants.VIEW_TITLE_CALC_RESULTS);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_HEADER, Constants.VIEW_PAGE_TITLE_HEADER_CALC_RESULTS);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER,
				Constants.VIEW_PAGE_TITLE_SUBHEADER_CALC_RESULTS);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_FLOT, true);

		logger.info("Form data:  " + wekaData + "\n");
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_WEKADATA, wekaData);
		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_PREDICTIONS_MAP, calcService.evaluate(wekaData, request));

		modelMap.addAttribute(Constants.VIEW_KEY_PAGE_PDF_LOCATION,
				pdfController.getPdfFilePath(wekaData, request, response));

		return Constants.URL_CALC_RESULTS;
	}

}
