package com.giltroymeren.bosom2.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import com.giltroymeren.bosom2.domain.WekaData;
import com.giltroymeren.bosom2.service.CalcService;
import com.giltroymeren.bosom2.util.Constants;

public class CalcControllerTest {
	@InjectMocks
	private CalcController calcController;

	@Mock
	private WekaData wekaData;

	@Mock
	private ModelMap modelMap;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private BindingResult bindingResult;

	@Mock
	private CalcService calcService;

	@Mock
	private Map<String, Map<String, Map<String, Object>>> predictionsMap;

	@Mock
	private PdfController pdfController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		modelMap = new ModelMap();
	}

	@Test
	public void testShowGet_shouldContainCalcFormData() {
		assertEquals(Constants.URL_CALC_FORM, calcController.showGet(wekaData, modelMap, request, response));
		assertEquals(modelMap.get(Constants.VIEW_KEY_TTILE), Constants.VIEW_TITLE_CALC);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_NAME), Constants.VIEW_PAGE_NAME_CALC);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_HEADER), Constants.VIEW_PAGE_TITLE_HEADER_CALC);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER), Constants.VIEW_PAGE_TITLE_SUBHEADER_CALC);
	}

	/**
	 * TODO: testShowPost_hasNoErrors_shouldShowResultsPage
	 */

	@Test
	public void testShowPost_hasErrors_shouldShowFormPage() throws Exception {
		when(bindingResult.hasErrors()).thenReturn(true);

		assertEquals(Constants.URL_CALC_FORM,
				calcController.showPost(wekaData, bindingResult, modelMap, request, response));
		assertEquals(modelMap.get(Constants.VIEW_KEY_TTILE), Constants.VIEW_TITLE_CALC);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_HEADER), Constants.VIEW_PAGE_TITLE_HEADER_CALC);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER), Constants.VIEW_PAGE_TITLE_SUBHEADER_CALC);

		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_ALERT_STRONG_CONTENT),
				Constants.VIEW_PAGE_ALERT_STRONG_CONTENT);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_ALERT_CONTENT), Constants.VIEW_PAGE_ALERT_CONTENT);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_ALERT_TYPE), Constants.VIEW_PAGE_ALERT_TYPE);
	}

	@After
	public void tearDown() {
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_NAME), Constants.VIEW_PAGE_NAME_CALC);
	}
}
