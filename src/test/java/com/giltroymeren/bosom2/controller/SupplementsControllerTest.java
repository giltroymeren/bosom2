package com.giltroymeren.bosom2.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ModelMap;

import com.giltroymeren.bosom2.util.Constants;

public class SupplementsControllerTest {
	@InjectMocks
	private SupplementsController supplementsController;

	private ModelMap modelMap;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		modelMap = new ModelMap();
	}

	@Test
	public void testShowSupplementsPage_shouldContainSupplementsPageData() {
		assertEquals(Constants.URL_SUPPLEMENTS, supplementsController.showSupplementsPage(modelMap));
		assertEquals(modelMap.get(Constants.VIEW_KEY_TTILE), Constants.VIEW_TITLE_SUPPLEMENTS);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_NAME), Constants.VIEW_PAGE_NAME_SUPPLEMENTS);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_HEADER), Constants.VIEW_PAGE_TITLE_HEADER_SUPPLEMENTS);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER),
				Constants.VIEW_PAGE_TITLE_SUBHEADER_SUPPLEMENTS);
	}
}
