package com.giltroymeren.bosom2.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ModelMap;

import com.giltroymeren.bosom2.util.Constants;

public class AboutControllerTest {
	@InjectMocks
	private AboutController aboutController;

	private ModelMap modelMap;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		modelMap = new ModelMap();
	}

	@Test
	public void testShowAboutBosomPage_shouldContainAboutBosomData() {
		assertEquals(Constants.URL_ABOUT_BOSOM, aboutController.showAboutBosomPage(modelMap));
		assertEquals(modelMap.get(Constants.VIEW_KEY_TTILE), Constants.VIEW_TITLE_ABOUT_BOSOM);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_NAME), Constants.VIEW_PAGE_NAME_ABOUT);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_HEADER), Constants.VIEW_PAGE_TITLE_HEADER_ABOUT_BOSOM);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER),
				Constants.VIEW_PAGE_TITLE_SUBHEADER_ABOUT_BOSOM);
	}

	@Test
	public void testShowAboutSitePage_shouldContainAboutSiteData() {
		assertEquals(Constants.URL_ABOUT_SITE, aboutController.showAboutSitePage(modelMap));
		assertEquals(modelMap.get(Constants.VIEW_KEY_TTILE), Constants.VIEW_TITLE_ABOUT_SITE);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_NAME), Constants.VIEW_PAGE_NAME_ABOUT);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_HEADER), Constants.VIEW_PAGE_TITLE_HEADER_ABOUT_SITE);
		assertEquals(modelMap.get(Constants.VIEW_KEY_PAGE_TITLE_SUBHEADER),
				Constants.VIEW_PAGE_TITLE_SUBHEADER_ABOUT_SITE);
	}
}
