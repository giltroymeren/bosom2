<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>

<div class="jumbotron" id="page-index">
	<div class="overlay">
		<h1>Welcome</h1>
		<p class="lead">This is the Breast Cancer Outcome - Survival Online
			Measurement Calculator's website.</p>
		<p>
			<a class="btn btn-lg btn-info" href="<c:url value="/about"/>">Learn
				more &rarr;</a>
		</p>
	</div>
</div>

<div class="row marketing hide">
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>