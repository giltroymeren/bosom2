<div class="modal fade" id="modal-m3" tabindex="-1" role="dialog" aria-labelledby="modal-m3-label"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal-m3-label">
                    Metastasis <br /> <small> <code>Adjusted AJCC M 6th edition</code>,
                        SEER 18
                    </small>
                </h4>
            </div>

            <div class="modal-body">
                The information below were taken from <a target="_blank"
                    href="http://seer.cancer.gov/seerstat/variables/seer/ajcc-stage/6th/breast.html#m">
                    "Adjusted AJCC 6 M (1988+)", <em>Breast Schema for 1988+ based on AJCC 6th
                        edition</em>
                </a>

                <hr />

                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code>M0</code></td>
                                <td>No distant metastasis</td>
                            </tr>
                            <tr>
                                <td><code>M1</code></td>
                                <td>Distant metastasis</td>
                            </tr>
                            <tr>
                                <td><code>MX</code></td>
                                <td>Distant metastasis cannot be assessed</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ext2" tabindex="-1" role="dialog"
    aria-labelledby="modal-ext2-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal-ext2-label">
                    Extension of tumor <br /> <small> <code>EOD 10 - extend
                            (1988-2003)</code>, SEER 18
                    </small>
                </h4>
            </div>

            <div class="modal-body">
                The information below were taken from <a target="_blank"
                    href="http://seer.cancer.gov/archive/manuals/EOD10Dig.pub.pdf"> "Breast
                    Extension", <em>SEER Extent of Disease -- 1988: Codes and Coding
                        Instructions, 1998</em>
                </a> (page 110).

                <hr />

                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code>00</code></td>
                                <td>IN SITU: Noninfiltrating; intraductal <br /> WITHOUT
                                    infiltration; lobular neoplasia
                                </td>
                            </tr>
                            <tr>
                                <td><code>05</code></td>
                                <td>Paget's disease (WITHOUT underlying tumor)</td>
                            </tr>
                            <tr>
                                <td><code>10</code></td>
                                <td>Confined to breast tissue and fat including nipple and/or
                                    areola</td>
                            </tr>
                            <tr>
                                <td><code>11</code></td>
                                <td>Entire tumor reported as invasive (no in situ component
                                    reported)</td>
                            </tr>
                            <tr>
                                <td><code>13</code></td>
                                <td>Invasive and in situ components present, size of invasive
                                    component stated and coded in tumor Size</td>
                            </tr>
                            <tr>
                                <td><code>14</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND in situ described as minimal (less than 25%)</td>
                            </tr>
                            <tr>
                                <td><code>15</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND in situ described as extensive (25% or more)</td>
                            </tr>
                            <tr>
                                <td><code>16</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND proportions of in situ and invasive not known</td>
                            </tr>
                            <tr>
                                <td><code>17</code></td>
                                <td>Invasive and in situ components present, unknown size of
                                    tumor (Tumor Size coded <code>999</code>)
                                </td>
                            </tr>
                            <tr>
                                <td><code>18</code></td>
                                <td>Unknown if invasive and in situ components present, unknown
                                    if tumor size represents mixed tumor or a "pure" tumor</td>
                            </tr>
                            <tr>
                                <td><code>20</code></td>
                                <td>Invasion of subcutaneous tissue <br /> Skin infiltration
                                    of primary breast including skin of nipple and/or areola <br />
                                    Local infiltration of dermal lymphatics adjacent to primary
                                    tumor involving skin by direct extension
                                </td>
                            </tr>
                            <tr>
                                <td><code>21</code></td>
                                <td>Entire tumor reported as invasive (no in situ component
                                    reported)</td>
                            </tr>
                            <tr>
                                <td><code>23</code></td>
                                <td>Invasive and in situ components present, size of invasive
                                    component stated and coded in tumor Size</td>
                            </tr>
                            <tr>
                                <td><code>24</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND in situ described as minimal (less than 25%)</td>
                            </tr>
                            <tr>
                                <td><code>25</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND in situ described as extensive (25% or more)</td>
                            </tr>
                            <tr>
                                <td><code>26</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND proportions of in situ and invasive not known</td>
                            </tr>
                            <tr>
                                <td><code>27</code></td>
                                <td>Invasive and in situ components present, unknown size of
                                    tumor (Tumor Size coded <code>999</code>)
                                </td>
                            </tr>
                            <tr>
                                <td><code>28</code></td>
                                <td>Unknown if invasive and in situ components present, unknown
                                    if tumor size represents mixed tumor or a "pure" tumor</td>
                            </tr>
                            <tr>
                                <td><code>30</code></td>
                                <td>Invasion of (or fixation to) pectoral fascia or muscle;
                                    deep fixation; attachment or fixation to pectoral muscle or
                                    underlying tissue</td>
                            </tr>
                            <tr>
                                <td><code>31</code></td>
                                <td>Entire tumor reported as invasive (no in situ component
                                    reported)</td>
                            </tr>
                            <tr>
                                <td><code>33</code></td>
                                <td>Invasive and in situ components present, size of invasive
                                    component stated and coded in tumor Size</td>
                            </tr>
                            <tr>
                                <td><code>34</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND in situ described as minimal (less than 25%)</td>
                            </tr>
                            <tr>
                                <td><code>35</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND in situ described as extensive (25% or more)</td>
                            </tr>
                            <tr>
                                <td><code>36</code></td>
                                <td>Invasive and in situ components present, size of entire
                                    tumor coded in Tumor Size (size of invasive component not
                                    stated) AND proportions of in situ and invasive not known</td>
                            </tr>
                            <tr>
                                <td><code>37</code></td>
                                <td>Invasive and in situ components present, unknown size of
                                    tumor (Tumor Size coded <code>999</code>)
                                </td>
                            </tr>
                            <tr>
                                <td><code>38</code></td>
                                <td>Unknown if invasive and in situ components present, unknown
                                    if tumor size represents mixed tumor or a "pure" tumor</td>
                            </tr>
                            <tr>
                                <td><code>40</code></td>
                                <td>Invasion of (or fixation to) chest wall, ribs, intercostal
                                    or serratus anterior muscles</td>
                            </tr>
                            <tr>
                                <td><code>50</code></td>
                                <td>Extensive skin involvement: Skin edema, peau d'orange,
                                    "pigskin", en cuirasse, lenticular nodule(s), inflammation of
                                    skin, erythema, ulceration of skin of breast, satellite
                                    nodule(s) in skin of primary breast</td>
                            </tr>
                            <tr>
                                <td><code>60</code></td>
                                <td>(<code>50</code>) + (<code>40</code>)
                                    <ul>
                                        <li>Extensive skin involvement: Skin edema, peau
                                            d'orange, “pigskin,” en cuirasse, lenticular
                                            nodule(s), inflammation of skin, erythema, ulceration of
                                            skin of breast, satellite nodule(s) in skin of primary
                                            breast</li>
                                        <li>Invasion of (or fixation to) chest wall, ribs,
                                            intercostal or serratus anterior muscles</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><code>70</code></td>
                                <td>Inflammatory carcinoma, incl. diffuse (beyond that directly
                                    overlying the tumor) dermal lymphatic permeation or infiltration
                                </td>
                            </tr>
                            <tr>
                                <td><code>80</code></td>
                                <td>FURTHER contiguous extension: Skin over sternum, upper
                                    abdomen, axilla or opposite breast</td>
                            </tr>
                            <tr>
                                <td><code>85</code></td>
                                <td>Metastasis:
                                    <ul>
                                        <li>Bone, other than adjacent rib</li>
                                        <li>Lung</li>
                                        <li>Breast, contralateral - if stated as metastatic</li>
                                        <li>Adrenal gland</li>
                                        <li>Ovary</li>
                                        <li>Satellite nodule(s) in skin other than primary
                                            breast</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><code>99</code></td>
                                <td>UNKNOWN if extension or metastasis</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
