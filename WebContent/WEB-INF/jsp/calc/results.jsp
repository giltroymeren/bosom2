<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/page-header.jsp"%>

<div class="row">

    <div class="col-xs-12 col-sm-2 col-md-3 bosom-scrollspy-content">
    
        <ul class="nav nav-tabs nav-stacked scrollspy-nav" id="about-nav">
            <li>
                <a href="#entered-data" class="list-group-item stack-first"
                    aria-label="Results: Entered data"
                    data-tooltip="Results: Entered data"
                    title="Results: Entered data">
                    Entered data
                </a>
            </li>

            <li>
                <a href="#predicted-survival" class="list-group-item"
                    aria-label="Results: Table for predicted survival"
                    data-tooltip="Results: Table for predicted survival"
                    title="Results: Table for predicted survival">
                    Table for predicted survival
                </a>
            </li>

            <li>
                <a href="#graph-for-predicted-survival"class="list-group-item"
                    aria-label="Results: Graph for predicted survival"
                    data-tooltip="Results: Graph for predicted survival"
                    title="Results: Graph for predicted survival">
                    Graph for predicted survival
                </a> 
            </li>

            <li>
                <a href="#predictive-modeling" class="list-group-item"
                  aria-label="About: Predictive modeling"
                  data-tooltip="About: Predictive modeling"
                  title="About: Predictive modeling">
                  Predictive modeling
                </a>  
            </li>

            <li>
                <a href="#export-as-pdf" class="list-group-item stack-last"
                    aria-label="Results: Export as PDF"
                    data-tooltip="Results: Export as PDF"
                    title="Results: Export as PDF">
                    Export results as PDF
                </a>
            </li>
        </ul>
    </div>

    <div class="col-xs-12 col-sm-10 col-md-9 bosom-scrollspy-content">

        <div class="bosom-section" id="entered-data">
            <h2>Entered data</h2>
            <p>Here are the breast cancer values you provided in the calculator.</p>

            <div class="table-responsive">
                <table class="table table-hover table-striped table-bordered cell-vertical-middle">
                    <thead>
                        <tr>
                            <th>&#35;</th>
                            <th>Variable</th>
                            <th>Value provided</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Age of patient at diagnosis</td>
                            <td>
                                <c:out value="${wekaData.ageDiagNum}" />
                            </td>
                        </tr>

                        <tr>
                            <td>2</td>
                            <td>Race of patient</td>
                            <td>
                                <c:out value="${wekaData.raceGroup}" />
                            </td>
                        </tr>

                        <tr>
                            <td>3</td>
                            <td>Cancer stage (AJCC 6th Edition)</td>
                            <td>
                                <c:out value="${wekaData.stage3}" />
                            </td>
                        </tr>

                        <tr>
                            <td>4</td>
                            <td>Presence of distant
                        metastasis (M of TNM staging 6th edition)</td>
                            <td>
                                <c:out value="${wekaData.m3}" />
                            </td>
                        </tr>

                        <tr>
                            <td>5</td>
                            <td>Reason for no cancer surgery</td>
                            <td>
                                <c:out value="${wekaData.reasonNoCancerSurg}" />
                            </td>
                        </tr>

                        <tr>
                            <td>6</td>
                            <td>Extension</td>
                            <td>
                                <c:out value="${wekaData.ext2}" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>   

        <div class="bosom-section" id="predicted-survival">
            
            <h2>Table for predicted survival</h2>
            <p>
                Here are the predicted survivals as determined by our models
                based from past breast cancer patient records.
                These are from two to ten years, with two years of interval for uniformity. 
            </p>

            <div class="table-responsive">
                <table class="table table-hover table-striped table-bordered  cell-vertical-middle">
                    <thead>
                        <tr>
                            <th>Time period</th>
                            <th>Prediction<br>model</th>
                            <th>Predicted<br>survival (%)</th>
                            <th>Mean of predicted <br>survivals (%)</th>
                        </tr>
                    </thead>

                    <tbody>
                      <c:forEach items="${predictionsMap}" var="predictionsMap" varStatus="predictionsLoop">
                      
                          <c:forEach items="${predictionsMap.value}" var="model" varStatus="modelLoop">
                          <tr>
                              <c:choose>
                                  <c:when test="${modelLoop.count < 2}">
                                      <td rowspan="5">${predictionsLoop.count * 2} years</td>
                                  </c:when>
                              </c:choose>
  
                                  <td style="text-transform:uppercase">${model.key}</td>
                                  <td>
                                     <fmt:formatNumber type="number" 
                                        maxFractionDigits="2" minFractionDigits="2"
                                        value="${model.value.Percentage * 100}" />
                                  </td>
  
                              <c:choose>
                                  <c:when test="${modelLoop.count < 2}">
                                     <c:choose>
                                            <c:when test="${predictionsLoop.count == 1}">
                                               <td rowspan="5" id="results-time-2">
                                                   ${wekaData.time2}
                                               </td>
                                            </c:when>
                                            <c:when test="${predictionsLoop.count == 2}">
                                               <td rowspan="5" id="results-time-4">
                                                   ${wekaData.time4}
                                               </td>
                                            </c:when>
                                            <c:when test="${predictionsLoop.count == 3}">
                                               <td rowspan="5" id="results-time-6">
                                                   ${wekaData.time6}
                                               </td>
                                            </c:when>
                                            <c:when test="${predictionsLoop.count == 4}">
                                               <td rowspan="5" id="results-time-8">
                                                   ${wekaData.time8}
                                               </td>
                                            </c:when>
                                            <c:when test="${predictionsLoop.count == 5}">
                                               <td rowspan="5" id="results-time-10">
                                                   ${wekaData.time10}
                                               </td>
                                            </c:when>
                                     </c:choose>   
                                  </c:when>
                              </c:choose>
                          </tr>
                          </c:forEach>

                      </c:forEach>
                    
                    </tbody>
                </table>
            </div>  
        </div>

        <div class="bosom-section" id="graph-for-predicted-survival">
            <h2>Graph for predicted survival</h2>
            
            <div id="results-graph-graph" style="min-height: 300px"></div>
            <div id="results-graph-legend"></div>
        </div>

        <div class="bosom-section" id="export-as-pdf">
            <h2>Export report as PDF file </h2>
            
            <p>
                Clicking the button below might do any of the following, based on your 
                browser, its version and your device:
            </p>
                
            <ul>
                <li>
                    open a new browser tab that will show the PDF file that you can choose
                    to save or print right away;
                </li>
                <li>
                    it will be automatically saved; or
                </li>
                <li>
                    a <code>Save As</code> prompt will ask you if you want to save the file in
                    a location in your machine.
                </li>
            </ul>

            <p class="callout callout-warning bg-warning text-warning">
                The generated PDF file is only available for each BOSOM form submission.
                Please download and save it in your device or take note of the results.
                It will not be available after you leave the Results page.
                You can always try again by answering the 
                <a href="<c:url value="/calc"/>">Calculator</a> again. 
            </p>

            <div class="form-group">
                 <a href="<c:out value="${pdfLocation}"/>" 
                    class="btn btn-info btn-lg btn-block"
                    target="_blank">
                    <span class="glyphicon glyphicon-save"></span>
                    View report in PDF
                </a>
            </div>

            <p>
               You can keep the file as a reference for further analysis and interpretation
               by a licensed oncologist or breast cancer specialist to help you understand
               and assess the results better. 
            </p>

        </div>

    <div class="clearfix"></div>
   
    </div> <!-- contents -->
    
<div class="clearfix"></div>

<c:set var="isFlotUsed" value="${isFlotUsed}" />
<c:choose>
    <c:when test="${isFlotUsed == true}">
        
    <script type="text/javascript">
    $(document).ready(function() {
       // flot.js
        var data = [ 
            { data: [["2", $('#results-time-2').html()]], color: "#009E73" },
            { data: [["4", $('#results-time-4').html()]], color: "#F0E442" },
            { data: [["6", $('#results-time-6').html()]], color: "#0072B2" },
            { data: [["8", $('#results-time-8').html()]], color: "#D55E00" },
            { data: [["10", $('#results-time-10').html()]], color: "#CC79A7" }
        ];
               
        var flotContainer = $('#results-graph-graph'); 
        $.plot(flotContainer, data, {
            xaxes: [
              { position: 'bottom', axisLabel: 'Time period (years)' }
            ],
            yaxes: [
              { position: 'left', axisLabel: 'Predicted survival (%)',
                axisLabelPadding: 10 }
            ],
            grid: { hoverable: true, show: true },
            series: {
                bars: { show: true, barWidth: 0.75, align: "center" }
            },
            xaxis: { mode: "categories" }
        });
    });
    </script>
    
    </c:when>
</c:choose> 

<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>