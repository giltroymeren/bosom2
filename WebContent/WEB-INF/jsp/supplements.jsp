<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/page-header.jsp"%>

<h2>Local hospitals and NGO's</h2>

<h3>Hospitals</h3>

<ul>
    <li>
        Cancer Institute, <a href="http://www.pgh.gov.ph/en/">University of the Philippines - Philippine General Hospital</a>   
        <ul>
            <li>
                <a href="https://www.facebook.com/pages/Philippine-General-Hospital/104067952962987">UP-PGH Facebook page</a> 
            </li>
        </ul>
    </li>
    
    <li>
        <a href="http://www.stlukesmedicalcenter.com.ph/aboutus/institutes/cancer-institute">Cancer Institute</a>, St. Luke's Medical Center
        <ul>
            <li>
                <a href="https://www.facebook.com/StLukesMedicalCenterOfficial">Facebook page</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="http://www.themedicalcity.com/services/centers_of_excellence/cancer-center/contact-details">Cancer Center</a>, The Medical City
        <ul>
            <li>
                <a href="https://www.facebook.com/MetroMedicalCenter">Facebook page</a>, as &quot;Metropolitan Medical Center&quot;  
            </li>
        </ul>
    </li>

    <li>
        <a href="http://www.usthospital.com.ph/bci/bci.php">Benavides Cancer Institute</a>, University of Sto. Tomas Hopsital
    </li>
</ul>

<h3>Non-governmental organizations</h3>
<ul>
    <li>
        Philippine Cancer Society Inc.
        <ul>
            <li>
                <a href="http://philcancer.org.ph/">official website</a> 
            </li>
            <li>
                <a href="https://www.facebook.com/pages/Philippine-Cancer-Society-INC/216156488399561">Facebook page</a>
            </li>
        </ul>
    </li>
    
    <li>
        Philippine Breast Cancer Network
        <ul>
            <li>
                <a href="http://www.pbcn.org/">official website</a>
            </li>
            <li>
                <a href="http://pbcn.blogspot.com/">official blog</a>
            </li>
            <li>
                <a href="https://www.facebook.com/groups/philippinebreastcancernetwork/">Facebook (support) group</a>
            </li>
        </ul>

    </li>

    <li>
        Philippine Foundation for Breast Cancer Inc.    
        <ul>
            <li>
                <a href="http://kasuso.org/">official website</a> 
            </li>
            <li>
                <a href="https://www.facebook.com/kasusongpinay">Facebook page</a> 
            </li>
        </ul>
    </li>

    <li>
        Cancer Treatment and Support Foundation Inc.
        <ul>
            <li>
                <a href="http://the-cancer-foundation.org/">official website</a>
            </li>
            <li>
                <a href="https://www.facebook.com/CancerTreatmentAndSupportFoundationInc">Facebook page</a>
            </li>
        </ul>

    </li>
    
    <li>
        ICanServe Foundation Inc.
        <ul>
            <li>
                <a href="http://www.icanservefoundation.org/">official website</a>
            </li>
            <li>
                <a href="http://www.icanserve-foundation.blogspot.com/">blog</a>
            </li>
            <li>
                <a href="https://www.facebook.com/pages/ICanServe-Foundation-Inc/183543015002337">Facebook page</a>
            </li>
            <li>
                <a href="https://twitter.com/icanserve">Twitter account</a>   

            </li>
        </ul>
    </li>
</ul>

<h3>Other helpful resources</h3>
<ul>
    <li>
        <a href="http://beatingcancers.rxpinoy.com/index.php">Beating Cancers</a> by RxPinoy
        <ul>
            <li>
                &quot;<a href="http://beatingcancers.rxpinoy.com/groups_local.php">Local Cancer Support Groups</a>&quot;
            </li>
        </ul>
    </li>
</ul>

<h2>International programs</h2>
<p>
    The following websites/organizations are listed in the spirit of providing additional information and resources for anyone interested in learning and understanding cancer and breast cancer. 
</p>
<p>
    These mostly provide general information pages containing symptoms, prevention and statistics while some have options to for direct contact through e-mail, calls and other means available. 
</p>

<ul>
    <li>
        <a href="http://www.cancer.gov/">National Cancer Institute</a>, National Institutes of Health, Department of Health and Human Services, USA
        <ul>
            <li>
                <a href="http://www.cancer.gov/cancertopics/types/breast">Breast cancer general organization</a> 
            </li>
        </ul>
    </li>

    <li>
        <a href="http://seer.cancer.gov/">Surveillance, Epidemiology and End Results program</a>, National Cancer Institute, National Institutes of Health, Department of Health and Human Services, USA
        <ul>
            <li>
                <a href="http://seer.cancer.gov/statfacts/html/breast.html">Breast cancer general information</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="http://www.breastcancer.org/">breastcancer.org</a>, Pennsylvania, USA
    </li>
    <li>
        <a href="http://www.nationalbreastcancer.org/">National Breast Cancer Foundation, Inc.</a>, Frisco, Texas, USA
    </li>
    <li>
        <a href="http://thebreastcancersite.greatergood.com/clickToGive/bcs/home">The Breast Cancer Site</a>, USA
    </li>
    <li>
        <a href="http://www.breastcancercare.org.uk/">Breast Cancer Care</a>, United Kingdom
    </li>
</ul>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>