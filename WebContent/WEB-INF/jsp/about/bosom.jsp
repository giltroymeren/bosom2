<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/page-header.jsp"%>

<div class="row">

	<div class="col-xs-12 col-sm-2 col-md-3 bosom-scrollspy-nav">

		<ul class="nav nav-tabs nav-stacked scrollspy-nav" id="about-nav">

            <li>
                <a href="#breast-cancer" class="list-group-item stack-first"
                    aria-label="About: Breast cancer"
                    data-tooltip="About: Breast cancer"
                    title="About: Breast cancer">
                    Breast cancer
                </a>
            </li>
            
            
            <li>
                <a href="#data-mining" class="list-group-item"
                  aria-label="About: Data mining"
                  data-tooltip="About: Data mining"
                  title="About: Data mining">
                  Data mining
                </a>  
            </li>
            
            <li>
                <a href="#seer-data" class="list-group-item stack-last"
                  aria-label="About: SEER data"
                  data-tooltip="About: SEER data"
                  title="About: SEER data">
                  SEER data
                </a>
            </li>

            <li>
                <a href="#predictive-survival" class="list-group-item"
                  aria-label="About: Predicting survival"
                  data-tooltip="About: Predicting survival"
                  title="About: Predicting survival">
                  Predicting survival
                </a>  
            </li>

		</ul>

	</div>
	
	<div class="col-xs-12 col-sm-10 col-md-9 bosom-scrollspy-content">

        <div class="bosom-section" id="breast-cancer">

            <h2>Breast Cancer</h2>
            <p>
                It starts from healthy breast cells that undergo mutation defects. 
                Normally, unhealthy and dead cells are either repaired or replaced completely
                in order to preserve the rest of the group but these "defected" cells
                continue to develop and eventually affecting the healthy cells. 
                This causes <em>tumors</em>, or the mass group of defected cells
                that if left untreated, could spread to the other parts of the body 
                [<a href="#bosom-ref-acs">1</a>].  
            </p>
            <p>
                Breast cancer has been found to be the leading type of cancer in women worldwide. 
                2012 Global Cancer (GLOBOCAN) statistics show that breast cancer scored the
                highest in incidence and second highest in mortality in both sexes 
                [<a href="#bosom-ref-globocan">2</a>].                   
            </p>
            <p>
                Efforts have been made worldwide to increase awareness of the public
                to the causes and preventions of this cancer. The challenge to eradicate the
                negative reputation of this disease has driven organizations and governments
                to encourage everyone to be proactive in dealing with breast cancer.  
                In relation, early detection has been found to be effective in treating early stages.
                Men and women who suspect to have abnormal lumps or feels pain in their
                breast area are advised to go see a specialist for proper diagnosis that could
                save their lives. 
            </p>
        </div>

        <div class="bosom-section" id="data-mining">

            <h2>Data mining</h2>
            <p>
                Data mining is the discipline of finding patterns and relationships within data or 
                records that could lead to a sensible purpose to help understanding the entire
                body of data. 
            </p>
            <p>
                Mathematical and computing algorithms are applied to data in order to obtain
                these patterns and relationships. The results could be in the form of a rule-bases system,
                mirroring a human's reasoning method, or with weights or scores, assigned 
                to the records and parameters with high significance in the dataset.
            </p>
            <p>
                Today, major websites like Facebook, Twitter and Google, employ large-scale servers
                to store data from users worldwide in real time. 
                Search strings, status posts, and tweets among others 
                are constantly analyzed to discover what the users are currently enjoying the most,
                for example. Results from this could be applied to add new features geared to
                improve their website's appeal to the public.
            </p>
        </div>

        <div class="bosom-section" id="seer-data">

            <h2>SEER data</h2>
            <p>
                Our breast cancer data comes from the Surveillance, Epidemiology, and End Results
                Program (SEER) of the National Cancer Institute of the US Department of Health and
                Human Services. SEER is the government agency responsible for collecting cancer
                data from key locations in the USA. These records are free for public access but
                an interested party must submit an accomplished research data-use agreement
                first to inform the agency of their intent.
            </p>
        </div>

        
        <div class="bosom-section" id="predictive-survival">

            <h2>Predicting survival</h2>
            <p>
                In order to predict a patient's survival, the SEER breast cancer data where
                analyzed by data mining algorithms. Survival within two, four, six, eight, and ten
                years where calculated from around 100,000 records between 1998 and 2003.
            </p>
            
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">References</div>
            
            <div class="panel-body">
                <ol>
                    <li id="bosom-ref-acs">
                        <p>
                            "<em>Breast Cancer</em>". American Cancer Society. 2013. 
                            Available from: 
                            <tt>
                                <a href="http://www.cancer.org/acs/groups/cid/documents/webcontent/003090-pdf.pdf">http://www.cancer.org/acs/groups/cid /documents/webcontent/003090-pdf.pdf</a>
                            </tt>. 
                            Accessed on 19 July 2013.
                        </p>
                    </li>
                    <li id="bosom-ref-globocan">
                        <p>
                            Ferlay J, Soerjomataram I, Ervik M, Dikshit R, Eser S, Mathers C, 
                            Rebelo M, Parkin DM, Forman D, Bray, F.
                            "<em>Population Fact Sheets</em>".
                            GLOBOCAN 2012 v1.0, Cancer Incidence and Mortality Worldwide: 
                            IARC CancerBase No. 11 [Internet].
                            Lyon, France: International Agency for Research on Cancer; 2013. 
                            Available from: 
                            <tt>
                                <a href="http://globocan.iarc.fr">
                                    http://globocan.iarc.fr
                                </a>
                            </tt>.
                            Accessed on 28 February 2014.
                        </p>
                    </li>
                </ol>
            </div>
            
        </div>
        
        
        <div class="clearfix"></div>

    </div>
    
    <div class="clearfix"></div>

    <%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>