<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/page-header.jsp"%>

<div class="row">

    <div class="col-xs-12 col-sm-2 col-md-3 bosom-scrollspy-nav">

        <ul class="nav nav-tabs nav-stacked scrollspy-nav" id="about-nav">
            <li><a href="#about-site-calculator" class="list-group-item stack-first"
                aria-label="About: Breast cancer calculator"
                data-tooltip="About: Breast cancer calculator"
                title="About: Breast cancer calculator"> Calculator </a></li>

            <li><a href="#about-site-backend" class="list-group-item"
                aria-label="About: Website backend" data-tooltip="About: Website backend"
                title="About: Website backend"> Website backend </a></li>

            <li><a href="#about-site-frontend" class="list-group-item"
                aria-label="About: Website frontend" data-tooltip="About: Website frontend"
                title="About: Website frontend"> Website frontend </a></li>

            <li><a href="#about-site-developer" class="list-group-item"
                aria-label="About: Developer" data-tooltip="About: Developer"
                title="About: Developer"> Developer </a></li>

        </ul>
    </div>

    <div class="col-xs-12 col-sm-10 col-md-9 bosom-scrollspy-content">

        <div class="bosom-section" id="about-site-calculator">

            <h2>Calculator</h2>
            <p>
                The breast cancer data from the <a href="http://seer.cancer.gov/">Surveillance,
                    Epidemiology, and End Results</a> program were used to create the models to
                calculate a prediction of a patient's survival.
            </p>

            <p>
                <img src="<c:url value="/resources/images/logos/weka.png"/>"
                    class="img-responsive img-inline-left" alt="WEKA logo"
                    data-image-source="http://www.cs.waikato.ac.nz/ml/weka/citing.html" /> The <a
                    href="http://www.cs.waikato.ac.nz/ml/">University of Waikato Machine
                    Learning Group</a>'s open-source machine learning software <a
                    href="http://www.cs.waikato.ac.nz/ml/weka/">Waikato Environment for
                    Knowledge Analysis</a> provided the tool to create models.
            </p>

            <p>The Java API has built-in algorithms and modules for preprocessing, modeling and
                forecasting that are helpful in general data mining and artificial intelligence
                projects. Its components are free to modify for more specific tasks that are not
                currently implemented in the official releases.</p>

        </div>

        <div class="bosom-section" id="about-site-backend">

            <h2>Website backend</h2>

            <h3>Framework</h3>
            <p>
                <img src="<c:url value="/resources/images/logos/spring-source.gif"/>"
                    class="img-responsive img-inline-left" alt="Spring Source logo"
                    data-image-source="http://blog.architexa.com/2012/10/the-decline-of-spring/" />

                The open source Java web framework <a href="http://spring.io/">Spring MVC</a> is
                used to serve the user interface and data from the WEKA models.
            </p>
            <p>This Java-based model-view-controller framework is known for its reliability and
                maintainability in development of websites proven by its &quot;separation of
                concern&quot; paradigm as seen in its components.</p>
            <p>
                Kindly refer to the <a
                    href="http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html">introductory
                    documentation</a> on its theoretical flow and principles and how to get started
                programming with Spring and the web.
            </p>

            <h3>Server</h3>
            <p>
                This website is hosted by University of the Philippines Manila's <a
                    href="http://agila.upm.edu.ph/"> Agila Computer Science Development Server </a>.
                More information can be found in its <a href="http://agila.upm.edu.ph/docs/doku.php">
                    wiki page </a>.
            </p>

        </div>

        <div class="bosom-section" id="about-site-frontend">

            <h2>Website frontend</h2>

            <h3>User interface</h3>
            <p>
                We employed <a href="http://getbootstrap.com/">Twitter's Bootstrap</a> user
                interface framework for faster and better deployment of the application given its
                variety of helper design components and interactive modules. User interaction and
                website visibility to most devices and browsers are greatly improved by this
                framework.
            </p>

            <h3>Graphs</h3>
            <p>
                The Calculator's graph in the results page is made using <a
                    href="http://www.flotcharts.org/">Flot</a>, a cross-browser interactive plotting
                JavaScript library. It is capable of generating various chart types - line, bar and
                pie and can be further expanded for more specific usage.
            </p>

            <h3>Images</h3>
            <p>Images seen in this site are not my property unless stated.</p>

            <ul>
                <li><strong><a href="http://nos.twnsnd.co/">New Old Stock</a></strong> provides
                    free vintage photos from public archives. Its usage policies are stated <a
                    href="http://www.flickr.com/commons/usage/">here</a>.</li>
                <li><strong><a href="http://unsplash.com/">Unsplash</a></strong> provides
                    high-resolution photos under the <a
                    href="http://creativecommons.org/publicdomain/zero/1.0/">&quot;Public Domain
                        Dedication&quot;</a> license.</li>
            </ul>

        </div>

        <div class="bosom-section" id="about-site-developer">

            <h2>Developer</h2>
            <p>I am Giltroy Meren and you can contact me through the following:</p>

            <ul>
                <li><strong>E-mail: </strong> <code>gpmeren+bosom@up.edu.ph</code></li>
            </ul>

        </div>

        <div class="panel panel-info">
            <div class="panel-heading">References</div>

            <div class="panel-body">
                <p>Additional credit to the images and other resources used in this specific
                    page.</p>

                <ul>
                    <li>
                        <p>
                            WEKA logo. "Citing Weka". <em>Waikato Enviroment for Knowledge
                                Analysis website</em>. Waikato Enviroment for Knowledge Analysis. Machine
                            Learning Group, Department of Computer Science, The University of
                            Waikato, New Zealand.
                            <tt>
                                <a href="http://www.cs.waikato.ac.nz/ml/weka/citing.html">
                                    href="http://www.cs.waikato.ac.nz/ml/weka/citing.html </a>
                            </tt>
                            .
                        </p>

                        <p>
                            Licensed under Creative Commons <a
                                href="http://creativecommons.org/licenses/by-sa/2.5/">
                                Attribution-ShareAlike 2.5 Generic </a>.
                        </p>
                    </li>

                    <li>
                        <p>
                            Spring Source logo. "The Decline Of Spring?". <em>Working with
                                Large Codebases</em>. Architexa, Inc.
                            <tt>
                                <a href="http://blog.architexa.com/2012/10/the-decline-of-spring/">
                                    http://blog.architexa.com/2012/10/the-decline-of-spring/ </a>
                            </tt>
                            .
                        </p>
                    </li>
                </ul>
            </div>

        </div>

        <div class="clearfix"></div>

    </div>

    <div class="clearfix"></div>

    <%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>