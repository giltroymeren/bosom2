<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>

<c:set var="title" value="This page does not exist" />
<c:set var="pageName" value="error" />

<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>

<div class="jumbotron jumbotron-error" id="page-404">
    <div class="overlay">
        <h1>This page does not exist.</h1>
        <p class="lead">
            The page you are trying to visit is not part of this website. 
            We have the <a href="#header-nav">navigation menu</a> at the top 
            and a <a href="#site-map">site map</a> in the bottom
            to help you go around this website.
        </p>
        <p>
            Please click <a href="/bosom">this link</a> to go back to the home page.
        </p>
    </div>
</div>

<div class="row hide">

<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>