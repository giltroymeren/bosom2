<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/page-header.jsp"%>

	<div class="list-group col-lg-12 col-xs-12">
		<h4>Part I</h4>
		<p>Vivamus ac quam libero. Nunc pellentesque faucibus nulla,
			imperdiet auctor nulla commodo non. Mauris pharetra gravida augue, ac
			convallis elit porttitor nec. Nam suscipit, lacus id dictum
			fermentum, nunc ante porta nisi, ac pretium lectus augue sed nisl.
			Proin eget dignissim sem. Vestibulum ante ipsum primis in faucibus
			orci luctus et ultrices posuere cubilia Curae; Nulla in dui ut diam
			cursus tristique sed nec massa. Sed ut mi nibh. Proin at congue
			libero, sit amet pellentesque diam. In nec nisi nec felis consequat
			rutrum non vitae leo. Duis vel turpis laoreet, vehicula elit nec,
			fringilla enim. Curabitur imperdiet tincidunt iaculis. Ut lobortis
			ante et neque aliquet aliquam.</p>

		<p>Nullam commodo massa in sollicitudin porta. Lorem ipsum dolor
			sit amet, consectetur adipiscing elit. Vestibulum venenatis elit
			tortor, sit amet vehicula mi fermentum et. Duis egestas tristique
			quam non venenatis. Nunc vulputate purus felis, in placerat odio
			bibendum vitae. Sed cursus gravida felis, sit amet gravida ligula
			placerat at. Nunc tincidunt dui vel dui faucibus eleifend. Phasellus
			tincidunt orci eu mi pellentesque blandit. Aliquam rutrum turpis nec
			libero faucibus, et tincidunt odio semper. Proin eleifend facilisis
			turpis. Donec quis massa eget felis suscipit convallis et eu risus.
			Aliquam scelerisque elit at erat congue semper. Class aptent taciti
			sociosqu ad litora torquent per conubia nostra, per inceptos
			himenaeos. Proin porttitor nisl venenatis lacus pellentesque commodo.</p>

		<p>Pellentesque sit amet elit felis. Mauris ac sapien molestie,
			dapibus dolor non, pretium ipsum. Etiam ornare, dolor nec pharetra
			imperdiet, risus massa bibendum nisl, sed congue diam erat non lorem.
			Fusce auctor arcu vel tempus dignissim. Sed auctor dapibus mauris,
			nec lobortis leo. Donec leo ipsum, aliquet et consectetur ut, sodales
			et mauris. Donec in justo id ligula imperdiet mattis. In tristique
			nec turpis ut dictum.</p>

		<h4>Part II</h4>
		<p>Pellentesque venenatis diam vitae lacinia tempus. Nunc pretium
			odio in nisl tempor, at pharetra elit tempus. Nulla venenatis
			facilisis felis vitae cursus. Sed feugiat lacus eget nunc venenatis
			tempor. Quisque nec dictum ante, ut viverra diam. Sed tincidunt, nunc
			et ultricies auctor, libero urna fermentum lectus, quis bibendum
			augue nibh eu mi. In a posuere lacus.</p>
	</div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>