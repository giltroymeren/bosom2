
</div>
<!--  end div.row -->

<div class="footer container-fluid">

    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-8 row">
            <h4>Information</h4>
            <p>
                Developed by <a href="https://twitter.com/troymeren">Troy Meren</a> &copy; 2013 -
                2014
            </p>
        </div>

        <div class="col-xs-6 col-md-4" id="site-map">

            <h4>Site Map</h4>

            <ul class="list-unstyled">
                <li><a href="<c:url value="/"/>" aria-label="Home" data-tooltip="Home"
                    title="Home"> Home </a></li>

                <li><a href="<c:url value="/about/bosom"/>" aria-label="About"
                    data-tooltip="About" title="About"> About </a>
                    <ul>
                        <li><a href="<c:url value="/about/bosom"/>">BOSOM Calculator</a></li>
                        <li><a href="<c:url value="/about/site"/>">BOSOM Site</a></li>
                    </ul></li>

                <li><a href="<c:url value="/calc"/>" aria-label="BOSOM Calculator"
                    data-tooltip="BOSOM Calculator" title="BOSOM Calculator"> BOSOM Calculator </a>
                </li>

                <li><a href="<c:url value="/supplements"/>" aria-label="Supplements"
                    data-tooltip="Supplements" title="Supplements"> Supplements </a></li>
            </ul>

        </div>

    </div>
</div>

</div>
<!-- div.container -->

<script type="text/javascript">
	$(document).ready(function() {
		// nasty scrollspy
		$(window).resize(function() {
			if ($(this).width() > 720) {
				$(".scrollspy-nav").affix({
					offset : {
						top : 15
					}
				});
			}
		});
	});
</script>

</body>
</html>