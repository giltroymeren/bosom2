<!DOCTYPE html>
<!--  

ooo        ooooo oooooooooooo ooooooooo.   oooooooooooo ooooo      ooo 
`88.       .888' `888'     `8 `888   `Y88. `888'     `8 `888b.     `8' 
 888b     d'888   888          888   .d88'  888          8 `88b.    8  
 8 Y88. .P  888   888oooo8     888ooo88P'   888oooo8     8   `88b.  8  
 8  `888'   888   888    "     888`88b.     888    "     8     `88b.8  
 8    Y     888   888       o  888  `88b.   888       o  8       `888  
o8o        o888o o888ooooood8 o888o  o888o o888ooooood8 o8o        `8  

-->
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>

<!-- Basic Page Needs -->
<meta charset="utf-8" />

<title>BOSOM <c:set var="title" value="${title}" /> <c:choose>
        <c:when test="${not empty title}"> 
            | <c:out value="${title}" />
        </c:when>
        <c:when test="${empty title}"> 
            | Welcome
        </c:when>
    </c:choose>
</title>

<meta name="description"
    content="Breast cancer prediction calculator using WEKA models and SEER data" />
<meta name="author" content="Troy Meren" />

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- Grand JavaScript -->
<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script type="text/javascript" src="<c:url value="/resources/js/html5shiv.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/respond.min.js"/>"></script>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<c:set var="isFlotUsed" value="${isFlotUsed}" />
<c:choose>
    <c:when test="${isFlotUsed == true}">
        <script type="text/javascript"
            src="<c:url value="/resources/js/flot.js/jquery.flot.min.js"/>"></script>
        <script type="text/javascript"
            src="<c:url value="/resources/js/flot.js/jquery.flot.categories.min.js"/>"></script>
        <script type="text/javascript"
            src="<c:url value="/resources/js/flot.js/jquery.flot.symbol.min.js"/>"></script>
    </c:when>
</c:choose>

<!--[if lte IE 8]>
    <script language="javascript" type="text/javascript" src="/resources/js/flot.js/excanvas.min.js"></script>
<![endif]-->

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />" />
<link rel="stylesheet" type="text/css"
    href="<c:url value="/resources/css/bootstrap-theme.min.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jumbotron-narrow.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/custom.css" />" />

<!-- Favicons -->
<link rel="shortcut icon" href="<c:url value="/resources/images/favicon.ico"/>" />
<link rel="apple-touch-icon" href="<c:url value="/resources/images/apple-touch-icon.png"/>" />
<link rel="apple-touch-icon" sizes="72x72"
    href="<c:url value="/resources/images/apple-touch-icon-72x72.png"/>" />
<link rel="apple-touch-icon" sizes="114x114"
    href="<c:url value="/resources/images/apple-touch-icon-114x114.png"/>" />

</head>

<body data-spy="scroll" data-target="#bosom-scrollspy">

    <div class="container container-fluid">

        <div class="header text-center">

            <h2 id="site-title">
                BOSOM <small> Breast Cancer Outcome - Survival Online Measurement Calculator
                </small>
            </h2>

            <ul class="nav nav-pills nav-justified" id="header-nav">

                <li
                    <c:if test="${empty pageName}">
                    class="active"
                    </c:if>><a
                    href="<c:url value="/"/>" aria-label="Home" data-tooltip="Home" title="Home">
                        Home </a></li>

                <li
                    class="dropdown <c:if test="${(pageName == 'about')}">
                    active
                    </c:if>
                ">
                    <a id="dropdown-about" role="button" data-toggle="dropdown"
                    href="<c:url value="/about/bosom"/>"> About <b class="caret"></b>
                </a>

                    <ul role="menu" class="dropdown-menu" aria-labelledby="dropdown-about">
                        <li role="presentation"><a role="menuitem"
                            href="<c:url value="/about/bosom"/>"> BOSOM Calculator </a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem"
                            href="<c:url value="/about/site"/>"> BOSOM Site </a></li>
                    </ul>
                </li>

                <li
                    <c:if test="${(pageName == 'calc')}">
                        class="active"
                    </c:if>>
                    <a href="<c:url value="/calc"/>" aria-label="BOSOM Calculator"
                    data-tooltip="BOSOM Calculator" title="BOSOM Calculator"> BOSOM Calculator </a>
                </li>

                <li
                    <c:if test="${(pageName == 'supplements')}">
                        class="active"
                    </c:if>>
                    <a href="<c:url value="/supplements"/>" aria-label="Supplements"
                    data-tooltip="Supplements" title="Supplements"> Supplements </a>
                </li>

            </ul>

        </div>