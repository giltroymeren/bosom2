<div class="page-header">
  <h1>
    <c:out value="${pageTitleHeader}"/>
    <small><c:out value="${pageTitleSubheader}"/></small>
  </h1>
</div>

<div class="row">